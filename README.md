Author:  Mike Ciavarella  
Title:  Configuration Management Class - Extras  

---
# Overview

This repository contains scripts, Ansible playbooks, and other files intended to
be used in conjunction with my Configuration Management class.  

You should:

1. Fork a copy of this repository into your own GitLab account.  More information
   can be found at https://docs.gitlab.com/ee/workflow/forking_workflow.html

2. Clone to your local machine for actual usage

## Contributing

Any pull requests will be considered, but please note that files
in this repository are _very_ specific to the class.  For example,
in some cases, there are deliberate errors in files, which students 
are expected to discover and fix.  Similarly, some files are 
incomplete.  Pull requests that seek to correct these 'bugs'
will be declined _because the 'bug' is intentionally present -- it's a **feature**._


# Index
